/*
 * This file is part of cflag-tester.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef JOBS_H
# define JOBS_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

# include <pthread.h>

typedef int (job_func_t)(void * data);

struct job_s {
    job_func_t * func;
    void * data;
};
typedef struct job_s job_t;

struct jobs_s {
    unsigned int jobs_len;
    job_t * jobs;

    pthread_mutex_t mutex;
    pthread_cond_t cond;
    unsigned int error;
    unsigned int next;
    unsigned int prog;
};
typedef struct jobs_s jobs_t;

int jobs_init(jobs_t * const jobs) __attribute__((nonnull));
void jobs_deinit(jobs_t * const jobs) __attribute__((nonnull));

int jobs_threads(jobs_t * const jobs, unsigned int threads)
    __attribute__((nonnull));

int jobs_append(jobs_t * const jobs,
                job_func_t * const func,
                void * const data)
    __attribute__((nonnull(1,2)));

int jobs_run(jobs_t * const jobs, const unsigned int n_threads)
    __attribute__((nonnull));

#endif /* !JOBS_H */
