/*
 * This file is part of cflag-tester.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef EXPECT_H
# define EXPECT_H

# if __GNUC__
#  define expect(x, y) __builtin_expect((x), (y))
# else /* !__GNUC__ */
#  define expect(x, y) (x)
# endif /* __GNUC__ */

# define likely(x)   expect(!!(x), 1)
# define unlikely(x) expect(!!(x), 0)

#endif /* !EXPECT_H */
