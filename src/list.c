/*
 * This file is part of cflag-tester.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdlib.h>
#include <string.h>

#include "expect.h"
#include "list.h"
#include "print-error.h"

#define SEP " ,"

#define LIST_FROM_STRING_APPEND(memb) \
    do { \
        list = realloc(list, (len+1) * sizeof(char *)); \
        if (unlikely(list == NULL)) { \
            print_error_errno("could not allocate memory"); \
            free(copy); \
            return NULL; \
        } \
        list[len++] = memb; \
    } while (false);


char ** __attribute__((nonnull))
list_from_string(const char * string, size_t * const lenp)
{
    string += strspn(string, SEP);

    char * copy = strdup(string);
    if (unlikely(copy == NULL)) {
        print_error_errno("could not allocate memory");
        return NULL;
    }

    char * end;
    char ** list = NULL;
    size_t len = 0;
    while ((end = strpbrk(copy, SEP)) != NULL) {
        *end = '\0';
        LIST_FROM_STRING_APPEND(copy);
        copy = end + 1;
        copy += strspn(copy, SEP);
    }
    if (*copy != '\0') {
        LIST_FROM_STRING_APPEND(copy);
    }

    if (unlikely(len == 0)) {
        free(copy);
        list = malloc(sizeof(char *));
        if (list == NULL) {
            print_error_errno("could not allocate memory");
            return NULL;
        }
        list[0] = NULL;
    }

    *lenp = len;
    return list;
}

void
list_free(char ** const list)
{
    if (likely(list != NULL)) {
        free(list[0]);
        free(list);
    }
    return;
}
