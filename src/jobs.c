/*
 * This file is part of cflag-tester.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

#include "expect.h"
#include "jobs.h"
#include "print-error.h"

int __attribute__((nonnull))
jobs_init(jobs_t * const jobs)
{
    jobs->jobs_len = 0;
    jobs->jobs = NULL;

    if (pthread_cond_init(&jobs->cond, NULL)) {
        print_error_errno("could not initialize condition variable");
        pthread_mutex_destroy(&jobs->mutex);
        return 1;
    }

    if (pthread_mutex_init(&jobs->mutex, NULL)) {
        print_error_errno("could not initialize mutex");
        return 1;
    }

    jobs->error = 0;
    jobs->next = 0;
    jobs->prog = 0;

    return 0;
}

void __attribute__((nonnull))
jobs_deinit(jobs_t * const jobs)
{
    pthread_cond_destroy(&jobs->cond);
    pthread_mutex_destroy(&jobs->mutex);
    free(jobs->jobs);
    return;
}

int __attribute__((nonnull(1,2)))
jobs_append(jobs_t * const jobs, job_func_t * const func, void * const data)
{
    jobs->jobs = realloc(jobs->jobs, (jobs->jobs_len + 1) * sizeof(job_t));
    if (unlikely(jobs->jobs == NULL)) {
        print_error_errno("could not allocate memory");
        return 1;
    }

    job_t * const job = jobs->jobs + jobs->jobs_len;
    job->func = func;
    job->data = data;
    jobs->jobs_len++;

    return 0;
}

static void __attribute__((nonnull))
jobs_error(jobs_t * const jobs)
{
    pthread_mutex_lock(&jobs->mutex);
    jobs->error++;
    pthread_cond_signal(&jobs->cond);
    pthread_mutex_unlock(&jobs->mutex);
    return;
}

static void * __attribute__((nonnull))
job_thread(void * const data)
{
    jobs_t * const jobs = data;

    bool first = true;
    while (true) {
        job_t * job;

        pthread_mutex_lock(&jobs->mutex);
        if (!first) {
            jobs->prog++;
            pthread_cond_signal(&jobs->cond);
        }
        if (jobs->next < jobs->jobs_len) {
            job = jobs->jobs + jobs->next++;
        } else {
            job = NULL;
        }
        if (jobs->error) {
            job = NULL;
        }
        pthread_mutex_unlock(&jobs->mutex);

        if (job == NULL) {
            return NULL;
        } else if ((*job->func)(job->data)) {
            goto error;
        }

        first = false;
    }

error:
    __attribute__((cold));
    jobs_error(jobs);
    return NULL;
}

static unsigned int __attribute__((const))
digits(unsigned int n)
{
    unsigned int i = 0;
    do {
        n /= 10;
        i++;
    } while (n);
    return i;
}

static void
jobs_prog(const unsigned int prog, const unsigned int max)
{
    assert(prog <= max);

    unsigned int width = 73;
    const unsigned int max_digits = digits(max);
    width -= 2 * max_digits;
    fputs("\r ", stdout);
    for (unsigned int i = digits(prog); i < max_digits; ++i) {
        fputc(' ', stdout);
    }
    printf("%u / %u [", prog, max);

    unsigned int i = (prog * width) / max;
    width -= (i > 1) ? i : 1;
    while (i-- > 1) {
        fputc('=', stdout);
    }
    fputc('>', stdout);
    while (width-- > 0) {
        fputc(' ', stdout);
    }
    fputc(']', stdout);

    fflush(stdout);

    return;
}

int __attribute__((nonnull))
jobs_run(jobs_t * const jobs, const unsigned int n_threads)
{
    if (unlikely(jobs->jobs_len == 0)) {
        return 0;
    }

    int retval = 0;

    pthread_t threads[n_threads];
    for (unsigned int i = 0; i < n_threads; ++i) {
        if (pthread_create(threads + i, NULL, &job_thread, jobs)) {
            print_error_errno("could not create thread");
            fputs("waiting for threads to finish...\n", stderr);
            jobs_error(jobs);
            for (unsigned int j = 0; j < i; ++j) {
                pthread_join(threads[j], NULL);
            }
            return 1;
        }
    }

    const unsigned int max = jobs->jobs_len;

    jobs_prog(0, max);

    unsigned int prog = 0;
    while (prog < max) {
        pthread_mutex_lock(&jobs->mutex);
        pthread_cond_wait(&jobs->cond, &jobs->mutex);
        if (jobs->error) {
            putchar('\n');
            print_error("job failed");
            fputs("waiting for threads to finish...\n", stderr);
            goto error;
        }
        prog = jobs->prog;
        pthread_mutex_unlock(&jobs->mutex);

        jobs_prog(prog, max);
    }
    putchar('\n');

    if (false) {
    error:
        retval = 1;
    }

    for (unsigned int i = 0; i < n_threads; ++i) {
        pthread_join(threads[i], NULL);
    }

    return retval;

}
