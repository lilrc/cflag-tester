/*
 * This file is part of cflag-tester.
 * 
 * Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <gop.h>

#include "expect.h"
#include "jobs.h"
#include "list.h"
#include "print-error.h"

static char * default_flags_string = NULL;
static char * f_flags_string = NULL;
static char * march_flags_string = NULL;

static gop_return_t __attribute__((nonnull))
print_version(gop_t * const gop)
{
    const char * const license = "\
License GPLv3+: GNU GPL version 3 or later\n\
<http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n";

    printf("%s\n"
           "Copyright (C) 2014 Karl Linden\n"
           "%s", PACKAGE_STRING, license);
    return GOP_DO_EXIT;
}

static void
free_strings(void)
{
    free(default_flags_string);
    free(f_flags_string);
    free(march_flags_string);
    return;
}

static gop_return_t
atexit_cb(gop_t * const gop, void * const ptr)
{
    free_strings();
    return GOP_DO_EXIT;
}

int __attribute__((nonnull))
main(const int argc, const char ** const argv)
{
    int exit_status = EXIT_SUCCESS;

    jobs_t jobs;
    jobs_init(&jobs);

    unsigned int threads = 1;

    const gop_option_t options[] = {
        {"default-flags", '\0', GOP_STRING, &default_flags_string, NULL,
            "comma separated list of default flags", "DEFAULT_FLAGS"},
        {"f-flags", '\0', GOP_STRING, &f_flags_string, NULL,
            "comma separated list of -f flags", "F_FLAGS"},
        {"march-flags", '\0', GOP_STRING, &march_flags_string, NULL,
            "comma separated list of -march flags to test", "MARCH_FLAGS"},
        {"threads", 't', GOP_UNSIGNED_INT, &threads, NULL,
            "number of build threads to run", "THREADS"},
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_AUTOHELP,
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (unlikely(gop == NULL)) {
        goto error;
    }

    gop_set_program_name(gop, PACKAGE_NAME);
    gop_add_option_table(gop, "Program options:", options);
    gop_atexit(gop, &atexit_cb, NULL);
    gop_parse_options(gop, argv);
    gop_get_unpaired_arguments(gop, argv, (size_t)(argc + 1));
    gop_destroy(gop);

    if (unlikely(threads == 0)) {
        print_error("threads may not be zero");
        goto error;
    }

    /* FIXME: Append some build jobs. */

    jobs_run(&jobs, threads);

    if (false) {
    error:
        exit_status = EXIT_FAILURE;
    }

    jobs_deinit(&jobs);
    free_strings();
    return exit_status;
}
